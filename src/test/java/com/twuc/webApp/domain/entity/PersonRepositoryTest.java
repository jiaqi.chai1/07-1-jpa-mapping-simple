package com.twuc.webApp.domain.entity;

import com.twuc.webApp.entity.Person;
import com.twuc.webApp.entity.PersonRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class PersonRepositoryTest {

    @Autowired
    PersonRepository repository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_person() {
        Person person = new Person(1, "Jack", 20);

        repository.save(person);

        entityManager.flush();
        entityManager.clear();

        Optional<Person> optional = repository.findById(1);
        assertThat(optional.isPresent()).isTrue();
    }
}
